'''
*_VENDOR_BINDER defines the vendor API to use for sourcing data
'''

STOCK_VENDOR_BINDER = 'yahoo'
NEWS_VENDOR_BINDER  = 'yahoo'
BOND_VENDOR_BINDER  = 'yahoo'


if STOCK_VENDOR_BINDER == 'yahoo':
    import vendor.yahoo_data as stock_data
if NEWS_VENDOR_BINDER == 'yahoo':
    import vendor.yahoo_data as news_data
if BOND_VENDOR_BINDER == 'yahoo':
    import vendor.yahoo_data as bond_data


class DataConnector:

    def __init__(self):
        self._stock_data = stock_data
        self._news_data = news_data
        self._bond_data = bond_data

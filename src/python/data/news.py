from data.connector import DataConnector

'''
    All data in this class will be returned as DataFrames? News?
'''

class News(DataConnector):

    def __init__(self, ticker=None):
        super().__init__()
        self._ticker = ticker

    def get_news(self):
        return self._stock_data.news(self._ticker)

if __name__ == '__main__':
    from display.pretty_print import p_print
    s = News('AI')
    p_print(s.get_news())

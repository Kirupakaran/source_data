from data.connector import DataConnector

'''
    All data in this class will be returned as DataFrames indexed by date
'''


class Stock(DataConnector):

    def __init__(self, ticker=None):
        super().__init__()
        self._ticker = ticker

    def get_close_price(self):
        return self._stock_data.history(self._ticker)['Close']

if __name__ == '__main__':
    from display.pretty_print import p_print
    s = Stock('AI')
    p_print(s.get_close_price())

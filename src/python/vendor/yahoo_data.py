import yfinance as yf
import pandas as pd
from datetime import datetime


def history(ticker):
    return yf.Ticker(ticker).history()

'''
Column Index(['uuid', 'title', 'publisher', 'link', 'providerPublishTime', 'type']
Row Index - Date
'''
def news(ticker):
    news_list = yf.Ticker(ticker).news
    news_df = pd.DataFrame(news_list)

    news_df['date'] = news_df.providerPublishTime.apply(lambda timestamp: datetime.fromtimestamp(timestamp))
    news_df.set_index('date', inplace=True)
    return news_df

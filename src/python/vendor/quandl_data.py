import quandl as quandl
import numpy as np
import pandas as pd

'''
Key for Free Quandl access. 

Refer to below link for Quandl data organization and available data
for free account

https://docs.data.nasdaq.com/docs/data-organization
https://data.nasdaq.com/search

Quandl packages are now moved to nasdaq data link
pip install nasdaq-data-link
import nasdaqdatalink
'''
quandl.ApiConfig.api_key = 'azDakg2QHVBFwpJYzDDJ'

mydata = quandl.get("EIA/PET_RWTC_D")
#print(mydata)

#EDGA - BATS: https://en.wikipedia.org/wiki/BATS_Global_Markets
#mydata = quandl.get('BATS/EDGA_AAPL')
#mydata = quandl.get('BATS/EDGA_AAPL', start_date="2020-12-31", end_date="2021-12-31")
mydata = quandl.get_table('QUOTEMEDIA/PRICES', date='2022-4-6', ticker='AAPL')
print(mydata)